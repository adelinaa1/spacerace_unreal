Zero G VR locomotion template, by Nikolaus Stromberg  nikorasu85@gmail.com

You may use this freely, but please give me credit.
If you make something with this, I'd love to hear about it!

If you have any questions, feel free to ask me! I'll help if I can.


UPDATED 1/23/19

This version was updated for UE4.21.2

Also added a realistic bounce off surfaces and a boost/air-brake, and fixed a couple bugs.